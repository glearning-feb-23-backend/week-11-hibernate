package com.greatlearning.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.greatlearning.entity.Course;
import com.greatlearning.entity.Student;
import com.greatlearning.entity.Teacher;
import com.greatlearning.entity.TeacherDetails;

public class CreateTeacher {

	public static void main(String[] args) {
		
		//create session factory
		SessionFactory factory = new Configuration()
				                     .configure("hibernate.cfg.xml")
				                     .addAnnotatedClass(Teacher.class)
				                     .addAnnotatedClass(TeacherDetails.class)
				                     .addAnnotatedClass(Course.class)
				                     .addAnnotatedClass(Student.class)
				                     .buildSessionFactory();
		
		//create session
		Session session = factory.openSession();
		
		try {
			//create a student object
			System.out.println("Creating new teacher object...");
			//parent
			//transient object
			Teacher teacher1 = new Teacher("Sarath", "Binny", "sarath@gl.com");
			//Teacher teacher2 = new Teacher("Himanshu", "Kumar", "himanshu@gl.com");
			
			//child
			TeacherDetails teacherDetails = new TeacherDetails("Bangalore", "Karnataka");
			
			//create few courses;
			/*
			 * Course maths = new Course("Mathemetics", 3); Course science = new
			 * Course("Science", 6); Course english = new Course("English", 2);
			 */
			
			//start a transaction
			session.beginTransaction();
			
			//scaffolding code
			//teacher1.setTeacherDetails(teacherDetails);
			//teacherDetails.setTeacher(teacher1);
			//teacher1.addTeacherDetails(teacherDetails);
			
			/*
			 * teacher1.addCourse(maths); teacher1.addCourse(science);
			 * teacher1.addCourse(english);
			 */
	
			
			//save the student object
			System.out.println("saving the teacher ..."); 
			session.save(teacher1);
			teacher1.setEmail("hari@gmail.com");
			teacher1.setFirstName("hari");
			//session.save(teacher2);
			
			
			
			//commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
			
		}
		finally {
			session.close();
			factory.close();
		}
	}
}
