package com.greatlearning.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="teachers")
public class Teacher {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="email")
	private String email;
	
	//complex object
	/*
	 * @OneToOne(mappedBy = "teacher", cascade = CascadeType.ALL) private
	 * TeacherDetails teacherDetails;
	 */
	/*
	 * @OneToMany(mappedBy="teacher", cascade = CascadeType.ALL, fetch
	 * =FetchType.LAZY) private Set<Course> courses = new HashSet<>();
	 */
	
	public Teacher() {}
	
	public Teacher(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	/*
	 * public TeacherDetails getTeacherDetails() { return teacherDetails; }
	 * 
	 * public void setTeacherDetails(TeacherDetails teacherDetails) {
	 * this.teacherDetails = teacherDetails; }
	 */
	
	/*
	 * @OneToMany(mappedBy="teacher", cascade = CascadeType.ALL, fetch=
	 * FetchType.EAGER) public Set<Course> getCourses() { return courses; }
	 * 
	 * public void setCourses(Set<Course> courses) { this.courses = courses; }
	 */

	/*
	 * //scaffolding code to set both the sides of the relationship // this code
	 * should be written in the parent entity class public void
	 * addTeacherDetails(TeacherDetails teacherDetails) {
	 * this.setTeacherDetails(teacherDetails); teacherDetails.setTeacher(this); }
	 */
	/*
	 * public void addCourse(Course course) { this.courses.add(course);
	 * //course.setTeacher(this); }
	 */

	@Override
	public String toString() {
		return "Teacher [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}
	
}